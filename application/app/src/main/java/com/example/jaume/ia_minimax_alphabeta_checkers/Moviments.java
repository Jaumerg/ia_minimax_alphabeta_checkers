package com.example.jaume.ia_minimax_alphabeta_checkers;

// Guarda una taula de moviments
// a més de tots els moviments legals
// ens diu quants de moviments hi ha "numMoviments"
// i quants d'ells són captures "numSalts"

class Moviments
{
    int numMoviments;
    int numSalts;
    Moviment[] moviments = new Moviment[GeneradorDeMoviments.MAXMOVIMENTS];

    public Moviments()
    {
        inicia();
        for (int i=0; i<GeneradorDeMoviments.MAXMOVIMENTS;i++)
            moviments[i] = new Moviment(-1,-1);
    }

    public void inicia()
    {
        numMoviments = 0;
        numSalts = 0;
    }
}