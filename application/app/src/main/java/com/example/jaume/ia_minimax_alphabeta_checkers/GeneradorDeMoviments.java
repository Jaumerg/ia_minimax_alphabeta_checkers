package com.example.jaume.ia_minimax_alphabeta_checkers;
import android.graphics.Point;

public class GeneradorDeMoviments {

    static final int MAXMOVIMENTS = 36;  // màxim permès de moviments
    static final int INV = 33; // casella invàlida
    BitBoard t;
    Moviment movimentDeSalt;   // captures
    public static int MASK_L3, MASK_L5, MASK_R3, MASK_R5;


    // Un tauler es guardarà amb 32 bits, un per a cada casella:

    //  28 29 30 31
    // 24 25 26 27
    //  20 21 22 23
    // 16 17 18 19
    //  12 13 14 15
    // 08 09 10 11
    //  04 05 06 07
    // 00 01 02 03

    // Taula per trobar directament a quina casella podem moure segons el bit activat del bitboard
    // un tauler per cada direcció (dreta, esquerra,  esquerra arrera, dreta arrera)
    final int[] seguentCasella = {
            4, 5, 6, 7,
            9, 10, 11, INV,
            12, 13, 14, 15,
            17, 18, 19, INV,
            20, 21, 22, 23,
            25, 26, 27, INV,
            28, 29, 30, 31,
            INV, INV, INV, INV,

            INV, 4, 5, 6,
            8, 9, 10, 11,
            INV, 12, 13, 14,
            16, 17, 18, 19,
            INV, 20, 21, 22,
            24, 25, 26, 27,
            INV, 28, 29, 30,
            INV, INV, INV, INV,

            INV, INV, INV, INV,
            0, 1, 2, 3,
            INV, 4, 5, 6,
            8, 9, 10, 11,
            INV, 12, 13, 14,
            16, 17, 18, 19,
            INV, 20, 21, 22,
            24, 25, 26, 27,

            INV, INV, INV, INV,
            1, 2, 3, INV,
            4, 5, 6, 7,
            9, 10, 11, INV,
            12, 13, 14, 15,
            17, 18, 19, INV,
            20, 21, 22, 23,
            25, 26, 27, INV
    };


    public static char[] aBitCount = new char[65536];
    char[] aLowBit   = new char[65536];
    char[] aHighBit  = new char[65536];

    public GeneradorDeMoviments()
    {
        t = new BitBoard();
        init();
    }

    // Taules lookup per trobar el bit més significatiu, el menys significatiu i el número de bits a un enter de 16 bits

    void iniciaTaulesDeBits()
    {
        for (int movs = 0; movs < 65536; movs++)
        {
            char nBits = 0;
            char nLow = 255, nHigh = 255;
            for (int i = 0; i < 16; i++) {
                if ((movs & BitBoard.S[i])!=0)
                {
                    nBits++;
                    if (nLow == 255) nLow = (char) i;
                    nHigh = (char) i;
                }
            }
            aLowBit[movs] = nLow;
            aHighBit[movs] = nHigh;
            aBitCount[movs] = nBits;
        }
    }

    public void init()
    {
        // inicialització de màsqueres pels moviments possibles

        MASK_L3 = (BitBoard.S[ 1] | BitBoard.S[ 2] | BitBoard.S[ 3] | BitBoard.S[ 9] | BitBoard.S[10] | BitBoard.S[11] | BitBoard.S[17] | BitBoard.S[18] | BitBoard.S[19] | BitBoard.S[25] | BitBoard.S[26] | BitBoard.S[27]);
        MASK_L5 = (BitBoard.S[ 4] | BitBoard.S[ 5] | BitBoard.S[ 6] | BitBoard.S[12] | BitBoard.S[13] | BitBoard.S[14] | BitBoard.S[20] | BitBoard.S[21] | BitBoard.S[22]);
        MASK_R3 = (BitBoard.S[28] | BitBoard.S[29] | BitBoard.S[30] | BitBoard.S[20] | BitBoard.S[21] | BitBoard.S[22] | BitBoard.S[12] | BitBoard.S[13] | BitBoard.S[14] | BitBoard.S[ 4] | BitBoard.S[ 5] | BitBoard.S[ 6]);
        MASK_R5 = (BitBoard.S[25] | BitBoard.S[26] | BitBoard.S[27] | BitBoard.S[17] | BitBoard.S[18] | BitBoard.S[19] | BitBoard.S[ 9] | BitBoard.S[10] | BitBoard.S[11]);

        iniciaTaulesDeBits();
    }



    public BitBoard taulerABitboards(Tauler tauler) {
        // Hem de convertir el tauler de 8x8 als 3 BitBoard de peces
        // blanques, negres i dames

        // taula per a la conversió: 255 no existeix, la resta el número
        // de bit corresponent del bitboard

        int[] conversABitboard =
                        {   255,3,255,2,255,1,255,0,
                            7,255,6,255,5,255,4,255,
                            255,11,255,10,255,9,255,8,
                            15,255,14,255,13,255,12,255,
                            255,19,255,18,255,17,255,16,
                            23,255, 22,255,21,255,20,255,
                            255,27,255,26,255,25,255,24,
                            31,255, 30,255, 29,255, 28,255};


        int idx;
        BitBoard b = new BitBoard();
        b.clear();  // inicialitza el bitboard

        for (int f=0;f<8;f++) {
            for (int c = 0; c < 8; c++) {
                idx = f*8+c;
                //System.out.println("valor "+f+":"+c+" "+tauler.casella[f][c]);
                switch(tauler.casella[f][c])
                {
                    case Game.DAMABLANCA:  //si és una dama, es també del seu color
                        b.K |= 1<<conversABitboard[idx];
                    case Game.BLANCA:
                        //System.out.println("blanca a "+conversABitboard[idx]);
                        b.WP |= 1<<conversABitboard[idx];
                        break;
                    case Game.DAMANEGRA://si és una dama, es també del seu color
                        b.K |= 1<<conversABitboard[idx];
                    case Game.NEGRA:
                        //System.out.println("negra a "+conversABitboard[idx]);
                        b.BP |= 1<<conversABitboard[idx];
                        break;
                }
            }
        }
        b.casellesBuides =  ~(b.WP|b.BP);
        return b;
    }

    int[] convertirDeBitboard =
            {   7, 5, 3, 1,
                    16, 14, 12, 10,
                    27, 25, 23, 21,
                    36, 34, 32, 30,
                    47, 45, 43, 41,
                    56, 54, 52, 50,
                    67, 65, 63, 61,
                    76, 74, 72, 70};

    // Converteix un moviment del bitboard a les coordenades del tauler
    public Moviment convertirACoordenadesTauler(Moviment m) {
        Moviment mn = new Moviment();

            mn.origen = convertirDeBitboard[m.origen];
            mn.desti = convertirDeBitboard[m.desti];
            mn.salt = m.salt;
            mn.dama = m.dama;

            int idx = 0;
            while (m.cami[idx] != INV) {
                mn.cami[idx] = convertirDeBitboard[m.cami[idx]];
                idx++;
            }
            m.cami[idx] = INV;

        return mn;
    }

    // Converteix tots els moviments del bitboard a les coordenades del tauler
    public Moviments convertirACoordenadesTauler(Moviments m)
    {


        Moviments mn = new Moviments();

        mn.numMoviments = m.numMoviments;
        mn.numSalts = m.numSalts;

        for (int i=0; i<m.numMoviments; i++)
        {
            mn.moviments[i].origen = convertirDeBitboard[m.moviments[i].origen];
            mn.moviments[i].desti = convertirDeBitboard[m.moviments[i].desti];
            mn.moviments[i].salt = m.moviments[i].salt;
            int idx = 0;
            while (m.moviments[i].cami[idx] != INV) {
                mn.moviments[i].cami[idx] = convertirDeBitboard[m.moviments[i].cami[idx]];
                idx++;
            }
            m.moviments[i].cami[idx] = INV;
        }
        return mn;
    }

    // Genera tots els moviments de les peces d'un color
    public Moviments generaMovimentsPeces(Tauler tauler, int color)
    {
        BitBoard t = taulerABitboards(tauler);
        t.casellesBuides =  ~(t.WP | t.BP);
        movimentDeSalt= new Moviment(-1,-1);

        if (color == Game.BLANCA) return convertirACoordenadesTauler(cercaMovimentsBlanques(t));
        else return convertirACoordenadesTauler(cercaMovimentsNegres(t));
    }

    // Genera els moviments de les peces blanques
    public Moviments generaMovimentsBlanques(Tauler tauler)
    {
        BitBoard t = taulerABitboards(tauler);
        t.casellesBuides =  ~(t.WP | t.BP);
        movimentDeSalt= new Moviment(-1,-1);

        return convertirACoordenadesTauler(cercaMovimentsBlanques(t));
    }

    // Genera els moviments de les peces negres
    public Moviments generaMovimentsNegres(Tauler tauler)
    {
        BitBoard t = taulerABitboards(tauler);
        t.casellesBuides =  ~(t.WP | t.BP);
        movimentDeSalt= new Moviment(-1,-1);

        return convertirACoordenadesTauler(cercaMovimentsNegres(t));
    }

    public int movimentsSimplesBlanques(BitBoard t)
    {
        int WK =  (t.WP & t.K);
        int moviments = 0;

        moviments = ((t.casellesBuides << 4));
        moviments |= ((t.casellesBuides&MASK_L3) << 3);
        moviments |= ((t.casellesBuides&MASK_L5) << 5);
        moviments &= t.WP;
        if (WK!=0)
        {
            moviments |= (t.casellesBuides >>> 4) & WK;
            moviments |= ((t.casellesBuides&MASK_R3) >>> 3) & WK;
            moviments |= ((t.casellesBuides&MASK_R5) >>> 5) & WK;
        }

        return moviments;
    }

    public int movimentsSimplesNegres(BitBoard t)
    {

        int BK =  (t.BP & t.K);
        int moviments;

        moviments = ((t.casellesBuides >>> 4));
        moviments |= ((t.casellesBuides&MASK_R3) >>> 3);
        moviments |= ((t.casellesBuides&MASK_R5) >>> 5);
        moviments &= t.BP;
        if (BK!=0)
        {
            moviments |= (t.casellesBuides << 4) & BK;
            moviments |= ((t.casellesBuides&MASK_L3) << 3) & BK;
            moviments |= ((t.casellesBuides&MASK_L5) << 5) & BK;
        }
        return moviments;
    }

    public int mengenBlanques(BitBoard t)
    {
        int WK = (t.WP&t.K);
        int moviments = 0;
        int Temp = ((t.casellesBuides << 4) & t.BP);
        moviments |= (((Temp&MASK_L3) << 3) | ((Temp&MASK_L5) << 5));
        Temp =  ((((t.casellesBuides&MASK_L3) << 3) | ((t.casellesBuides&MASK_L5) << 5) ) & t.BP);
        moviments |= (Temp << 4);
        moviments &= t.WP;
        if (WK != 0) {
            Temp =  ((t.casellesBuides>>> 4) & t.BP);
            moviments |= (((Temp&MASK_R3) >>> 3) | ((Temp&MASK_R5) >>> 5)) & WK;
            Temp = ((((t.casellesBuides&MASK_R3) >>> 3) | ((t.casellesBuides&MASK_R5) >>> 5) ) & t.BP);
            moviments |= (Temp >>> 4) & WK;
        }
        return moviments;
    }

    public int mengenNegres(BitBoard t)
    {
        int BK = (t.BP&t.K);
        int moviments = 0;
        int Temp = ((t.casellesBuides >>> 4) & t.WP);
        moviments |= (((Temp&MASK_R3) >>>3) | ((Temp&MASK_R5) >>> 5));
        Temp =  ((((t.casellesBuides&MASK_R3) >>> 3) | ((t.casellesBuides&MASK_R5) >>> 5) ) & t.WP);
        moviments |= (Temp >>> 4);
        moviments &= t.BP;
        if (BK != 0) {
            Temp =  ((t.casellesBuides<< 4) & t.WP);
            moviments |= (((Temp&MASK_L3) << 3) | ((Temp&MASK_L5) << 5)) & BK;
            Temp = ((((t.casellesBuides&MASK_L3) << 3) | ((t.casellesBuides&MASK_L5) << 5) ) & t.WP);
            moviments |= (Temp << 4) & BK;
        }
        return moviments;
    }

    public void afegirBot(Moviments mvs, Moviment m, int pathNum)
    {
        assert(pathNum < 10 );
        assert(mvs.numMoviments < 36);

        m.cami[pathNum] = 33;
        mvs.moviments[mvs.numSalts].salt = true;
        mvs.moviments[mvs.numSalts].origen = m.origen;
        mvs.moviments[mvs.numSalts].desti = m.desti;
        for (int i=0; i<=pathNum;i++)
            mvs.moviments[mvs.numSalts].cami[i] = m.cami[i];
        //System.out.println("Bot: "+mvs.numSalts+" ("+m.origen+":"+m.desti+")");
        mvs.numSalts++;
    }

    public void afegirMoviment(Moviments m, int src, int dst, boolean dama)
    {
        assert( m.numMoviments < 36 );

        m.moviments[m.numMoviments] = new Moviment(src,dst);
        m.moviments[m.numMoviments].dama = dama;
        //System.out.println("Moviment: "+m.numMoviments+" ("+src+":"+dst+")");
        m.numMoviments++;
        return;
    }

    // Hem de guardar totes les caselles del camí cap on pot saltar
    public int saltaBlancDireccio(Moviments m, BitBoard t, int square, boolean isKing, int pathNum, int DIR_INDEX )
    {
        int jumpSquare = seguentCasella[square+DIR_INDEX];
        //System.out.println("saltaBlancaDireccio:"+jumpSquare+" "+square);

        if ( (BitBoard.S[jumpSquare] & t.BP) != 0 )
        {
            int	dstSq = seguentCasella[jumpSquare+DIR_INDEX];
            if ( (t.casellesBuides & BitBoard.S[dstSq]) != 0 )
            {
                // jump is possible in this direction, see if the piece can jump more times
                movimentDeSalt.cami[pathNum] = dstSq;

                //System.out.println("Dins path: "+dstSq+"  "+pathNum);

                continuaSaltsBlanc(m, t, dstSq, pathNum+1, jumpSquare, isKing );
                return 1;
            }
        }
        return 0;
    }

    public int saltaNegraDireccio(Moviments m, BitBoard t, int square, boolean isKing, int pathNum, int DIR_INDEX )
    {
        int jumpSquare = seguentCasella[square+DIR_INDEX];
        //System.out.println("saltaNegraDireccio:"+jumpSquare+" "+DIR_INDEX);
        if ( (BitBoard.S[jumpSquare] & t.WP) != 0 )
        {
            int	dstSq = seguentCasella[jumpSquare+DIR_INDEX];
            if ( (t.casellesBuides & BitBoard.S[dstSq]) != 0 )
            {
                // jump is possible in this direction, see if the piece can jump more times
                movimentDeSalt.cami[pathNum] = dstSq;
                //System.out.println("Dins path: "+dstSq+"  "+pathNum);

                continuaSaltsNegra(m, t, dstSq, pathNum+1, jumpSquare, isKing );
                return 1;
            }
        }

        return 0;
    }

    public void continuaSaltsBlanc(Moviments m, BitBoard t, int square, int pathNum, int jumpSquare, boolean isKing )
    {
        int oldBP = t.BP;
        t.BP ^= BitBoard.S[jumpSquare];
        //System.out.println("continuaSaltsBlanc:"+square);

        int numMoves = saltaBlancDireccio(m, t, square, isKing, pathNum, (2<<5) );
        numMoves += saltaBlancDireccio(m, t, square, isKing, pathNum, (3<<5) );

        if ( isKing )
        {
            numMoves += saltaBlancDireccio(m, t, square, isKing, pathNum, (0<<5) );
            numMoves += saltaBlancDireccio(m, t, square, isKing, pathNum, (1<<5) );
        }
        if (numMoves == 0) {
            afegirBot(m, movimentDeSalt, pathNum);
        }
        t.BP = oldBP;
    }

    public void continuaSaltsNegra(Moviments m, BitBoard t, int square, int pathNum, int jumpSquare, boolean isKing )
    {
        int oldBP = t.WP;
        t.WP ^= BitBoard.S[jumpSquare];

        //System.out.println("continuaSaltsNegra:"+square);

        int numMoves = saltaNegraDireccio(m, t, square, isKing, pathNum, (0<<5) );
        numMoves += saltaNegraDireccio(m, t, square, isKing, pathNum, (1<<5) );

        if ( isKing )
        {
            numMoves += saltaNegraDireccio(m, t, square, isKing, pathNum, (2<<5) );
            numMoves += saltaNegraDireccio(m, t, square, isKing, pathNum, (3<<5) );
        }
        if (numMoves == 0) {
            afegirBot(m, movimentDeSalt, pathNum);
        }
        t.WP = oldBP;
    }

    public void miraSaltBlancSegonsDireccio(Moviments m, BitBoard t, int square, int DIR_INDEX )
    {
        int jumpSquare = seguentCasella[square+DIR_INDEX];
        if ( (BitBoard.S[jumpSquare] & t.BP) != 0 ) // si hi ha un contrari en aquesta direccio
        {
            int	dstSq = seguentCasella[jumpSquare+DIR_INDEX];
            if ( (t.casellesBuides & BitBoard.S[dstSq]) != 0 ) // i després hi ha una casella buida
            {
                // Pot botar, ara mira si pot continuar botant
                movimentDeSalt.origen = square;
                movimentDeSalt.desti = dstSq;
                continuaSaltsBlanc(m, t, dstSq, 0, jumpSquare, ((BitBoard.S[square]&t.K) != 0) );
                //System.out.println("Moviment salt: "+m.numMoviments+"("+square+":"+dstSq+")");

            }
        }
    }

    public void miraSaltNegraSegonsDireccio(Moviments m, BitBoard t, int square, int DIR_INDEX )
    {
        int jumpSquare = seguentCasella[square+DIR_INDEX];
        if ( (BitBoard.S[jumpSquare] & t.WP) != 0 ) // si hi ha un contrari en aquesta direccio
        {
            int	dstSq = seguentCasella[jumpSquare+DIR_INDEX];
            if ( (t.casellesBuides & BitBoard.S[dstSq]) != 0 ) // i després hi ha una casella buida
            {
                // Pot botar, ara mira si pot continuar botant
                movimentDeSalt.origen = square;
                movimentDeSalt.desti = dstSq;
                continuaSaltsNegra(m, t, dstSq, 0, jumpSquare, ((BitBoard.S[square]&t.K) != 0) );
                //System.out.println(" t: "+m.numMoviments+"("+square+":"+dstSq+")");
            }
        }
    }

    public void cercaBotsBlanques(Moviments m, BitBoard t, int moven)
    {
        while (moven != 0)
        {
            int sq = menorBit(moven);
            moven ^= BitBoard.S[sq];

            int oldEmpty = t.casellesBuides;
            t.casellesBuides |= BitBoard.S[sq];

            // ara miram els bots en les quatre direccions (dues endavant)
            miraSaltBlancSegonsDireccio(m, t, sq, (2<<5) );
            miraSaltBlancSegonsDireccio(m, t, sq, (3<<5) );
            if ( (t.K & BitBoard.S[sq])!=0 )   // i dues endarrera si és una dama
            {
                miraSaltBlancSegonsDireccio(m, t, sq, (0<<5) );
                miraSaltBlancSegonsDireccio(m, t, sq, (1<<5) );
            }

            t.casellesBuides = oldEmpty;
        }
        m.numMoviments = m.numSalts;
    }

    public void cercaBotsNegres(Moviments m, BitBoard t, int moven)
    {
        while (moven != 0)
        {
            int sq = majorBit(moven);
            moven ^= BitBoard.S[sq];
            int oldEmpty = t.casellesBuides;
            t.casellesBuides |= BitBoard.S[sq];

            // ara miram els bots en les quatre direccions (dues endavant)
            miraSaltNegraSegonsDireccio(m, t, sq, (0<<5) );
            miraSaltNegraSegonsDireccio(m, t, sq, (1<<5) );
            if ( (t.K & BitBoard.S[sq])!=0 )   // i dues endarrera si és una dama
            {
                miraSaltNegraSegonsDireccio(m, t, sq, (2<<5) );
                miraSaltNegraSegonsDireccio(m, t, sq, (3<<5) );
            }

            t.casellesBuides = oldEmpty;
        }
        m.numMoviments = m.numSalts;
    }

    public Moviments cercaMoviments(BitBoard t, int torn)
    {
        t.casellesBuides =  ~(t.WP | t.BP);
        movimentDeSalt= new Moviment(-1,-1);

        if (torn == Game.BLANCA) return cercaMovimentsBlanques(t);
        else return cercaMovimentsNegres(t);
    }

    public Moviments cercaMovimentsBlanques(BitBoard t)
    {
        Moviments movimentsBlanques = new Moviments();
        int mengen = mengenBlanques(t);

        if (mengen != 0)
            cercaBotsBlanques(movimentsBlanques, t, mengen);
        else
            cercaSimplesBlanques(movimentsBlanques, t, movimentsSimplesBlanques(t));
        return movimentsBlanques;
    }


    public Moviments cercaMovimentsNegres(BitBoard t)
    {
        Moviments movimentsNegres = new Moviments();
        int mengen = mengenNegres(t);

        if (mengen != 0) {
            cercaBotsNegres(movimentsNegres, t, mengen);
        }
        else
            cercaSimplesNegres(movimentsNegres, t, movimentsSimplesNegres(t));

        return movimentsNegres;
    }

    public void afegirMovimentNormal(Moviments m, BitBoard t, int src, int dst, boolean esDama)
    {
        if ((BitBoard.S[dst] & t.casellesBuides) != 0 )
            afegirMoviment(m, src, dst, esDama);
    }

    public void cercaSimplesBlanques(Moviments m, BitBoard t, int movs )
    {
        m.inicia( );

        while (movs != 0)
        {
            int sq = menorBit(movs); // Agafa una peça de les que es poden moure
            movs^= BitBoard.S[sq];

            afegirMovimentNormal(m, t, sq, seguentCasella[sq+(2<<5)], false); // Mira els possibles moviments cap endavant
            afegirMovimentNormal(m, t, sq, seguentCasella[sq+(3<<5)], false);

            if ( (BitBoard.S[sq] & t.K) != 0 ) { // For Kings only
                afegirMovimentNormal(m, t, sq, seguentCasella[sq+(0<<5)],true); // Mira els possibles moviments cap endarrera
                afegirMovimentNormal(m, t, sq, seguentCasella[sq+(1<<5)],true);
            }
        }
    }

    public void cercaSimplesNegres(Moviments m, BitBoard t, int movs )
    {
        m.inicia( );

        while (movs != 0)
        {
            int sq = majorBit(movs); // Agafa una peça de les que es poden moure
            movs^= BitBoard.S[sq];

            afegirMovimentNormal(m, t, sq, seguentCasella[sq+(0<<5)], false); // Mira els possibles moviments cap endavant
            afegirMovimentNormal(m, t, sq, seguentCasella[sq+(1<<5)], false);

            if ( (BitBoard.S[sq] & t.K) != 0 ) { // For Kings only
                afegirMovimentNormal(m, t, sq, seguentCasella[sq+(2<<5)], true); // Mira els possibles moviments cap endarrera
                afegirMovimentNormal(m, t, sq, seguentCasella[sq+(3<<5)], true);
            }
        }
    }

    // Quants de 1s hi ha a un int (32 bits) ?
    public int numBits( int movs )
    {
        if (movs == 0) return 0;
        return  (aBitCount[(movs & 65535)] + aBitCount[((movs>>>16) & 65535)]);
    }

    // Quin és el bit a 1 menys significatiu d'un int (32 bits) ?
    public int menorBit(int movs)
    {
        if ((movs & 65535) != 0) return  aLowBit[(movs & 65535)];
        if (((movs>>>16) & 65535) != 0) return (aLowBit[((movs>>>16) & 65535)] + 16);
        return 0;
    }

    // Quin és el bit a 1 més significatiu d'un int (32 bits) ?
    public int majorBit(int movs)
    {
        if (((movs>>>16) & 65535)!=0) return (aHighBit[ ((movs>>>16) & 65535) ] + 16);
        if ((movs & 65535)!= 0) return (aHighBit[(movs & 65535)]);
        return 0;
    }
}

