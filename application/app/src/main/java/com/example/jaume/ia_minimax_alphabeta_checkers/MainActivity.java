package com.example.jaume.ia_minimax_alphabeta_checkers;
import android.app.Activity;
import android.os.Bundle;
import android.graphics.Point;
import android.view.Display;

public class MainActivity extends Activity {

    // La pantalla principal
    private MainView view;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Agafa el display amb els detalls de la pantalla
        Display display = getWindowManager().getDefaultDisplay();

        // Mira la resolució de la pantalla
        Point resolution = new Point();
        display.getSize(resolution);

        // Crea la vista per l'aplicació (la pantalla del nostre joc)
        view = new MainView(this, resolution.x, resolution.y);

        // Fes que sigui la vista de la nostra aplicació (Activity)
        setContentView(view);
    }

    // Si l'aplicació es pausa, pausa el fil (thread)
    @Override
    protected void onPause() {
        super.onPause();
        view.pause();
    }

    // Si l'aplicació es restaura, reinicia el fil
    @Override
    protected void onResume() {
        super.onResume();
        view.resume();
    }

    public void onBackPressed(){
        // Si es vol interrompre el bottor de fletxa enrere,
        // llevar el super.onBackPressed()

        super.onBackPressed();
    }
}

