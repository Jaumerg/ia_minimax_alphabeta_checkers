package com.example.jaume.ia_minimax_alphabeta_checkers;

import android.util.Log;

import com.example.jaume.ia_minimax_alphabeta_checkers.Game;

public class Cerca {
    private GeneradorDeMoviments g = new GeneradorDeMoviments();
    private final int MAX_DEPTH = 3;

    Moviment avaluaTauler(Tauler t, int torn)
    {
        Moviment best_move = Minimax(t, torn);
        return best_move;
    }

    /**
     * Aquesta funció minimax suposa que quan es crida es cerca maximitzar l'utilitat de l'agent
     * que l'ha cridat.
     * @param t
     * @param torn
     * @return
     */
    private Moviment Minimax(Tauler t, int torn){
        State estat_actual = new State(t, torn, null);
        State estat_maxima_utilitat = MaxValue(estat_actual, torn, Game.PERD, Game.GUANYA);
        return estat_maxima_utilitat.getMoviment();
    }

    private boolean isMaxDepth(int torn_estat, int torn_inicial){
        return torn_estat==torn_inicial+MAX_DEPTH;
    }

    private int calculaPuntuacioEstat(State estat, String function){
        if(estat.isFinalState()){
            switch(function){               // revisar si es correcte
                case "max":
                    return Game.GUANYA;
                case "min":
                    return Game.PERD;
            }
        }
        return estat.getPuntuacio();
    }

    private State MaxValue(State estat_actual, int torn_inicial, int alfa, int beta){
        State estat_temporal;
        Tauler tauler;
        Moviment moviment;
        if(estat_actual.isFinalState() || isMaxDepth(estat_actual.getTorn(), torn_inicial)){
            int puntuacio = calculaPuntuacioEstat(estat_actual, "max");
            estat_actual.setUtility(puntuacio);
            return estat_actual;
        }
        int utility = Game.PERD;
        Moviments possible_moves = estat_actual.movimentsValids();
        Moviment best_move = possible_moves.moviments[0];
        for(int i = 0; i < possible_moves.numMoviments; i++){
            moviment = possible_moves.moviments[i];
            tauler = Game.simulaMoviment(estat_actual.getTauler(), moviment);
            estat_temporal = new State(tauler, estat_actual.getTorn()+1, moviment);
            int temporal_state_utility = MinValue(estat_temporal, torn_inicial, alfa, beta).getUtility();
            if(temporal_state_utility>utility){
                utility = temporal_state_utility;
                best_move = possible_moves.moviments[i];
            }

            if(temporal_state_utility>=beta){ //poda
                estat_actual.setUtility(utility);
                estat_actual.setMoviment(best_move);
                return estat_actual;
            }

            if(temporal_state_utility>alfa){
                alfa = temporal_state_utility;
            }
        }
        estat_actual.setUtility(utility);
        estat_actual.setMoviment(best_move);
        return estat_actual;
    }

    private State MinValue(State estat_actual, int torn_inicial, int alfa, int beta){
        State estat_temporal;
        Tauler tauler;
        Moviment moviment;

        if(estat_actual.isFinalState() || isMaxDepth(estat_actual.getTorn(), torn_inicial)){
            int puntuacio = calculaPuntuacioEstat(estat_actual, "max");
            estat_actual.setUtility(puntuacio);
            return estat_actual;
        }
        int utility = Game.GUANYA;
        Moviments possible_moves = estat_actual.movimentsValids();
        Moviment best_move = possible_moves.moviments[0];
        for(int i = 0; i < possible_moves.numMoviments; i++){
            moviment = possible_moves.moviments[i];
            tauler = Game.simulaMoviment(estat_actual.getTauler(), moviment);
            estat_temporal = new State(tauler, estat_actual.getTorn()+1, moviment);
            int temporal_state_utility = MaxValue(estat_temporal, torn_inicial, alfa, beta).getUtility();

            if(temporal_state_utility<utility){
                utility=temporal_state_utility;
                best_move = possible_moves.moviments[i];
            }
            if(temporal_state_utility<=alfa){ //poda
                estat_actual.setUtility(utility);
                estat_actual.setMoviment(best_move);
                return estat_actual;
            }
            if(temporal_state_utility<beta){
                beta=temporal_state_utility;
            }
        }
        estat_actual.setUtility(utility);
        estat_actual.setMoviment(best_move);
        return estat_actual;
    }

}