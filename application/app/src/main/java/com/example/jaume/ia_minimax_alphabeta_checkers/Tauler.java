package com.example.jaume.ia_minimax_alphabeta_checkers;

import static com.example.jaume.ia_minimax_alphabeta_checkers.Game.*;

// Tenim dos formats de tauler, un de 8x8 amb totes les peces
// i un conjunt de bitboards (mapa de bits) amb les peces
// separades: blanques, negres i dames

public class Tauler {
    public int[][] casella = new int[8][8];
    public BitBoard bits = new BitBoard();

    public Tauler() {
        // buida el tauler
        for (int f = 0; f < 8; f++) {
            for (int c = 0; c < 8; c++) {
                casella[f][c] = Game.BUIDA;
            }
        }

        // les caselles blanques del tauler no s'empleen
        boolean fitxa = true;
        for (int f = 0; f < 8; f++) {
            for (int c = 0; c < 8; c++) {
                if (fitxa) casella[f][c] = Game.NOVALIDA; // blanca
                fitxa = !fitxa;
            }
            fitxa = !fitxa;
        }

        // col·loca les fitxes a les posicions inicials
        fitxa = true;
        for (int f = 5; f < 8; f++) {
            for (int c = 0; c < 8; c++) {
                if (fitxa) casella[f][c] = Game.BLANCA; // blanca
                fitxa = !fitxa;
            }
            fitxa = !fitxa;
        }

        fitxa = false;
        for (int f = 0; f < 3; f++) {
            for (int c = 0; c < 8; c++) {
                if (fitxa) casella[f][c] = NEGRA; // negre
                fitxa = !fitxa;
            }
            fitxa = !fitxa;
        }
    }

    public void posaPeça(int fila, int columna, int peça) {
        casella[fila][columna] = peça;
    }

    public Tauler duplica()
    {
        Tauler t = new Tauler();
        for (int f = 0; f < 8; f++) {
            for (int c = 0; c < 8; c++) {
                t.casella[f][c] = casella[f][c];
            }
        }
        return t;
    }

    public BitBoard taulerABitboards() {
        // Hem de convertir el tauler de 8x8 als 3 BitBoard de peces
        // blanques, negres i dames

        // taula per a la conversió: 255 no existeix, la resta el número
        // de bit corresponent del bitboard

        int[] conversABitboard =
                {255, 3, 255, 2, 255, 1, 255, 0,
                        7, 255, 6, 255, 5, 255, 4, 255,
                        255, 11, 255, 10, 255, 9, 255, 8,
                        15, 255, 14, 255, 13, 255, 12, 255,
                        255, 19, 255, 18, 255, 17, 255, 16,
                        23, 255, 22, 255, 21, 255, 20, 255,
                        255, 27, 255, 26, 255, 25, 255, 24,
                        31, 255, 30, 255, 29, 255, 28, 255};


        int idx;
        BitBoard b = new BitBoard();
        b.clear();  // inicialitza el bitboard

        for (int f = 0; f < 8; f++) {
            for (int c = 0; c < 8; c++) {
                idx = f * 8 + c;
                //System.out.println("valor "+f+":"+c+" "+tauler.casella[f][c]);
                switch (casella[f][c]) {
                    case Game.DAMABLANCA:  //si és una dama, es també del seu color
                        b.K |= 1 << conversABitboard[idx];
                    case Game.BLANCA:
                        //System.out.println("blanca a "+conversABitboard[idx]);
                        b.WP |= 1 << conversABitboard[idx];
                        break;
                    case Game.DAMANEGRA://si és una dama, es també del seu color
                        b.K |= 1 << conversABitboard[idx];
                    case Game.NEGRA:
                        //System.out.println("negra a "+conversABitboard[idx]);
                        b.BP |= 1 << conversABitboard[idx];
                        break;
                }
            }
        }

        b.casellesBuides = ~(b.WP | b.BP);
        return b;
    }
}

class BitBoard {
    // valors corresponents a cada un dels bits del tauler 2^n
    // es podria fer amb un for, però no és tant difícil :)
    static int[] S = {1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096,
            8192, 16384, 32768, 65536, 131072, 262144, 524288,
            1048576, 2097152, 4194304, 8388608, 16777216, 33554432,
            67108864, 134217728, 268435456, 536870912, 1073741824,
            -2147483648, 0, 0};

    // Màscares de bits per detectar fàcilment algunes característiques
    // Poden ser útils també per a l'avaluació
    final int MASK_TOP = S[28] | S[29] | S[30] | S[31] | S[24] | S[25] | S[26] | S[27] | S[20] | S[21] | S[22] | S[23];
    final int MASK_BOTTOM = S[0] | S[1] | S[2] | S[3] | S[4] | S[5] | S[6] | S[7] | S[8] | S[9] | S[10] | S[11];
    final int MASK_TRAPPED_W = S[0] | S[1] | S[2] | S[3] | S[4];
    final int MASK_TRAPPED_B = S[28] | S[29] | S[30] | S[31] | S[27];
    final int MASK_1ST = S[0] | S[1] | S[2] | S[3];
    final int MASK_2ND = S[4] | S[5] | S[6] | S[7];
    final int MASK_3RD = S[8] | S[9] | S[10] | S[11];
    final int MASK_4TH = S[12] | S[13] | S[14] | S[15];
    final int MASK_5TH = S[16] | S[17] | S[18] | S[19];
    final int MASK_6TH = S[20] | S[21] | S[22] | S[23];
    final int MASK_7TH = S[24] | S[25] | S[26] | S[27];
    final int MASK_8TH = S[28] | S[29] | S[30] | S[31];
    final int MASK_EDGES = S[24] | S[20] | S[16] | S[12] | S[8] | S[4] | S[0] | S[7] | S[11] | S[15] | S[19] | S[23] | S[27] | S[31];
    final int MASK_2CORNER1 = S[3] | S[7];
    final int MASK_2CORNER2 = S[24] | S[28];
    final int MASK_ODD_ROW = MASK_1ST | MASK_3RD | MASK_5TH | MASK_7TH;

    // un tauler per les peces blanques, un per les negres, les dames i les caselles buides
    int WP;
    int BP;
    int K;
    int evalDames;
    int casellesBuides; // ~(WP|BP);
    int[] nPeces = new int[2];

    public BitBoard() {
        // Tauler inicial
        WP = 0b11111111111100000000000000000000;
        BP = 0b00000000000000000000111111111111;
        K = 0;
        evalDames = 0;
        nPeces[Game.BLANCA] = 12;
        nPeces[Game.NEGRA] = 12;
        casellesBuides = ~(WP | BP);
    }

    public BitBoard(int W, int B, int D, int buides, int b, int n, int d) {
        WP = W;
        BP = B;
        K = D;
        casellesBuides = buides;
        nPeces[Game.BLANCA] = b;
        nPeces[Game.NEGRA] = n;
        evalDames = d;
    }

    // Fer una còpia del bitboard
    public BitBoard duplica() {
        return new BitBoard(WP, BP, K, casellesBuides, nPeces[Game.BLANCA], nPeces[Game.NEGRA], evalDames);
    }

    public void clear() {
        WP = 0;
        BP = 0;
        K = 0;
        nPeces[Game.BLANCA] = 0;
        nPeces[Game.NEGRA] = 0;
        casellesBuides = 0;
        evalDames = 0;
    }

    public void pintaBitBoard(String miss) {
        System.out.println(miss);
        pintaSimpleBitBoard(WP|BP|K,"Totes");
        /*
        // Si les volem pintar per separat
        pintaSimpleBitBoard(WP, "Blanques");
        pintaSimpleBitBoard(BP, "Negres");
        pintaSimpleBitBoard(K, "Dames");
        */
    }

    public int contaFiches(int tipo){
        int fiches = 0;
        if(tipo == Game.BLANCA) {
            fiches = this.WP;
        }
        if(tipo == Game.NEGRA){
            fiches = this.BP;
        }
        return numOfOnes(fiches);
    }

    private int numOfOnes(int number){
        int count = 0;
        while(number != 0){
            number = number & (number-1);
            count++;
        }
        return count;
    }

    public void pintaSimpleBitBoard(int b, String miss) {
        System.out.println(miss);
        String line = "";
        int inc = 0;
        for (int i = 31; i >= 0; i--) {
            if (inc % 4 == 0)

            {
                if (inc % 8 == 0)
                    line += "\n";
                else
                    line += "\n ";
            }
            inc++;
            if ((b & (1 << i)) != 0) line += "1 ";
            else line += "0 ";
        }

        System.out.println(line);
    }

    public int BitCount(int b) {
        if (b == 0) return 0;
        return GeneradorDeMoviments.aBitCount[(b & 65535)] + GeneradorDeMoviments.aBitCount[((b >>> 16) & 65535)];
    }

    public int CanWhiteCheckerMove(int checkers) {
        return ((casellesBuides << 4) & checkers) | ((((casellesBuides & GeneradorDeMoviments.MASK_L3) << 3) | ((casellesBuides & GeneradorDeMoviments.MASK_L5) << 5)) & checkers);
    }

    public int CanBlackCheckerMove(int checkers) {
        return ((casellesBuides >>> 4) & checkers) | ((((casellesBuides & GeneradorDeMoviments.MASK_R3) >> 3) | ((casellesBuides & GeneradorDeMoviments.MASK_R5) >> 5)) & checkers);
    }

    public int peça(int sq) {
        if ((S[sq] & BP) != 0) {
            return ((S[sq] & K) != 0) ? DAMANEGRA : NEGRA;
        }
        if ((S[sq] & WP) != 0) {
            return ((S[sq] & K) != 0) ? DAMABLANCA : BLANCA;
        }

        return BUIDA;
    }
}

