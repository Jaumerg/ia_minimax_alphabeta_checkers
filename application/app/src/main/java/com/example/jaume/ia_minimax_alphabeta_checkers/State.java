package com.example.jaume.ia_minimax_alphabeta_checkers;

import android.util.Log;

public class State {

    private Tauler tauler;
    private int torn;
    private int utility;
    private Moviment moviment;

    private GeneradorDeMoviments g = new GeneradorDeMoviments();

    public State(Tauler t, int torn, Moviment moviment){
        this.tauler = t;
        this.torn = torn;
        this.utility = 0;
        this.moviment = moviment;
    }

    public int getUtility(){
        return this.utility;
    }

    public int getTorn(){
        return this.torn;
    }

    public Tauler getTauler(){
        return this.tauler;
    }

    public Moviment getMoviment(){
        return this.moviment;
    }

    // TO DO: comprobar si el estado actual, el tablero actual, ha terminado el juego
    public boolean isFinalState(){
        try {
            g.generaMovimentsPeces(this.tauler, this.torn);
            return false;
        }catch(Exception e){
            return true;
        }
    }

    public void setUtility(int utility){
        this.utility = utility;
    }

    public void setMoviment(Moviment moviment){
        this.moviment = moviment;
    }

    public Moviments movimentsValids(){
        return g.generaMovimentsPeces(this.tauler, this.torn);
    }

    // Aquesta funcio retorna la diferencia de peses que hi ha entre els dos colors
    // si es positiu significaque les negres tenen mes punts i si està en negatiu que les blanques
    // tenen mes punts
    public int getPuntuacio(){
        Tauler t = this.getTauler();
        BitBoard b = t.taulerABitboards();
        int blanques = b.contaFiches(Game.BLANCA);
        int negres = b.contaFiches(Game.NEGRA);
        return negres - blanques;
    }

}
