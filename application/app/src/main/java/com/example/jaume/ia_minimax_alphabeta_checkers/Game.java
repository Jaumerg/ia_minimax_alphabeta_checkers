package com.example.jaume.ia_minimax_alphabeta_checkers;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Typeface;
import android.media.AudioTrack;
import android.view.SurfaceHolder;

public class Game {
    public static final int BUIDA = -1;
    public static final int NOVALIDA = -2;
    public static final int NEGRA = 0;
    public static final int BLANCA = 1;
    public static final int DAMANEGRA = 2;
    public static final int DAMABLANCA = 3;
    public static final int TRANSPARENT = 10;
    public static final int TAULER = 4;
    public static final int PERD = -99999;
    public static final int GUANYA = 99999;
    public static String[] nomPeces = {"NEGRES","BLANQUES"};

    private Typeface fontJoc;
    private Context context;
    private int amplaPantalla;
    private int altPantalla;
    private Canvas canvas;

    private int ordenador = NEGRA;   // amb quin color juga l'ordenador
    private int comencen  = BLANCA;  // quin color comença
    private Paint paint = new Paint();
    private Point center;
    private Image[] images = new Image[5];

    private int grandariaCasella;   // ampla i alt de cada casella del tauler
    private int xTauler;            // posicions x i y del tauler pel dibuix
    private int yTauler;
    private int grandariaTauler;    // grandaria de tot el tauler
    private int margeTauler = 50;

    private boolean arrossegant;
    private boolean obligaMoviment;   // estàn en una seqüencia de menjar
    private int movimentObligat;      // casella d'on s'ha de moure

    private int quiJuga; // per controlar el torn

    private int fila = -1;      // fila i columna seleccionades per moure
    private int columna = -1;
    private int fitxa = -1;     // fitxa seleccionada per moure

    private int xCursor = -1;         // posició del cursor
    private int yCursor = -1;
    private int despX, despY;         // desplaçament on selecciona la fitxa

    private static int[] filaFinal = {7,0};  // quina és la darrera fila per a cada color

    private Tauler t;   // guarda la informació del tauler
    private Cerca cerca = new Cerca();  // s'encarrega de fer la cerca
    GeneradorDeMoviments g = new GeneradorDeMoviments();  // s'encarrega de generar els moviments legals
    private Moviments[] movimentsPeces = new Moviments[2]; // moviments per les peces blanques i negres
    private Moviment darrerMoviment = new Moviment();  // darrer moviment que s'ha fet, només per pintar-lo

    public Game(Context context, MainView m, int screenWidth, int screenHeight)
    {
        fontJoc = Typeface.createFromAsset(context.getAssets(), "fonts/arkitechbold.ttf");

        this.context = context;

        this.amplaPantalla = screenWidth;
        this.altPantalla = screenHeight;

        center = new Point(screenWidth/2, screenHeight/2);

        images[NEGRA]      = new Image(m, R.drawable.negre);
        images[BLANCA]     = new Image(m, R.drawable.blanca);
        images[DAMANEGRA]  = new Image(m, R.drawable.damanegra);
        images[DAMABLANCA] = new Image(m, R.drawable.damablanca);
        images[TAULER]     = new Image(m, R.drawable.tauler);

        t = new Tauler();

        movimentsPeces[BLANCA] = g.generaMovimentsPeces(t, BLANCA);
        movimentsPeces[NEGRA]  = g.generaMovimentsPeces(t, NEGRA);
        arrossegant = false;      // per controlar quan l'usuari mou una peça
        obligaMoviment = false;   // si hi ha una captura, és obligatòria matar
        quiJuga = contrari(comencen);  // com que canviaTorn controla els moviments, obligatòriament l'hem de cridar al principi
        canviaTorn();
    }

    public void update(long fps)
    {

    }

    // Pinta la pantalla fps vegades per segon

    public void draw(SurfaceHolder holder)
    {
        if (holder.getSurface().isValid()) {
            // Si la superficie de dibuix és vàlida, la bloquejam per pintar
            canvas = holder.lockCanvas();

            // Fons de la pantalla de color negre
            canvas.drawColor(Color.argb(255, 0, 0, 0));

            pintaTauler();

            // Ja hem pintat, desbloqueja la pantalla
            holder.unlockCanvasAndPost(canvas);
        }
    }

    // Pinta el tauler
    public void pintaTauler()
    {
        // el pintarem de tot l'ample de la pantalla i centrat verticalment

        // quina grandaria pot tenir cada casella ?
        grandariaCasella = (amplaPantalla-margeTauler*2)/8;
        grandariaTauler = grandariaCasella*8;
        xTauler = margeTauler;

        // quant d'espai hem de deixar adalt ?

        yTauler = (altPantalla-grandariaCasella*8)/2 - margeTauler;

        images[TAULER].draw(canvas, 0, yTauler-margeTauler, amplaPantalla, amplaPantalla);

        boolean blanca = true;
        paint.setStyle(Paint.Style.FILL);

        for (int f=0; f<8; f++) {
            for (int c = 0; c < 8; c++) {
                if (blanca)
                    paint.setColor(Color.WHITE);
                else
                    paint.setColor(Color.BLACK);

                // Si la casella forma part del darrer moviment, pinta-la de verd
                int casella = f*10+c;
                if (darrerMoviment.origen == casella || darrerMoviment.desti == casella)
                {
                    paint.setColor(Color.rgb(0, 255, 0));
                }

                // Pinta la casella
                canvas.drawRect(xTauler + c * grandariaCasella,
                        yTauler + f * grandariaCasella,
                        xTauler + c * grandariaCasella + grandariaCasella,
                        yTauler + f * grandariaCasella + grandariaCasella, paint);

                // Si hi ha una peça, la pintam
                if (t.casella[f][c] != BUIDA && t.casella[f][c]!=TRANSPARENT && esCasellaValida(f,c)) {
                    images[t.casella[f][c]].draw(canvas, xTauler + c * grandariaCasella,
                            yTauler + f * grandariaCasella,
                            grandariaCasella,
                            grandariaCasella);
                }

                blanca = !blanca; // colors de caselles alternatius a la fila
            }
            blanca = !blanca; // colors de caselles alterantius a principi de columna
        }

        // Si movem la fitxa interactivament, la pintam
        if (arrossegant) {
            images[fitxa].draw(canvas, xCursor-despX,
                    yCursor-despY,
                    grandariaCasella,
                    grandariaCasella);
        }
        else {
            // pinta els moviments que es poden fer
            if (quiJuga == BLANCA)
                pintaMoviments(movimentsPeces[BLANCA], Color.RED);
            else
                pintaMoviments(movimentsPeces[NEGRA], Color.YELLOW);
        }

        // Missatge de partida acabada (el moviment que té el torn no pot moure més)
        if (movimentsPeces[quiJuga].numMoviments == 0)
        {
            drawCenteredTextYTOP(canvas, "Guanyen les "+nomPeces[contrari(quiJuga)], 50*amplaPantalla/1080, Color.RED, 120);
        }
        else
            drawCenteredTextYTOP(canvas, "Juguen les "+nomPeces[quiJuga], 50*amplaPantalla/1080, Color.RED, 120);

    }

    // Pinta els possibles moviments amb línies
    public void pintaMoviments(Moviments m, int color)
    {
        for (int i = 0; i < m.numMoviments; i++) {
            int mitja = grandariaCasella / 2;

            Point p1 = new Point(xTauler + (m.moviments[i].origen % 10) * grandariaCasella + mitja,
                    yTauler + (m.moviments[i].origen / 10) * grandariaCasella + mitja);

            Point p2 = new Point(xTauler + (m.moviments[i].desti % 10) * grandariaCasella + mitja,
                    yTauler + (m.moviments[i].desti / 10) * grandariaCasella + mitja);
            Point p3 = new Point();

            // p4 si volem que la linia no arribi al final
            Point p4 = new Point();

            double factor;
            if (Math.abs(m.moviments[i].origen%10-m.moviments[i].desti%10) > 1) factor = 0.95;
            else factor = 0.85;

            p4.x = (int) (p1.x+factor*(p2.x-p1.x));
            p4.y = (int) (p1.y+factor*(p2.y-p1.y));

            paint.setColor(color);
            paint.setStrokeWidth(6);
            if (m.moviments[i].cami[0] != GeneradorDeMoviments.INV) {
                canvas.drawLine(p1.x, p1.y, p2.x, p2.y, paint);
                canvas.drawCircle(p2.x, p2.y, 8, paint);
            }
            else{
                canvas.drawLine(p1.x, p1.y, p4.x, p4.y, paint);
                canvas.drawCircle(p4.x, p4.y, 8, paint);
            }

            int j = 0;
            while (m.moviments[i].cami[j] != GeneradorDeMoviments.INV) {
                p3.x =xTauler + (m.moviments[i].cami[j] % 10) * grandariaCasella + mitja;
                p3.y =yTauler + (m.moviments[i].cami[j] / 10) * grandariaCasella + mitja;

                j++;

                canvas.drawLine(p2.x,
                        p2.y,
                        p3.x,
                        p3.y, paint);
                p2.x = p3.x;
                p2.y = p3.y;
            }
        }
    }

    public boolean esCasellaValida(Point c)
    {
        if (c.x != -1 && t.casella[c.x][c.y] != NOVALIDA) return true;
        else return false;
    }

    public boolean esCasellaValida(int x, int y)
    {
        if (x != -1 && t.casella[x][y] != NOVALIDA) return true;
        else return false;
    }

    public boolean dinsTauler(int x, int y)
    {
        return (x >= xTauler && x <= xTauler+grandariaTauler && y>=yTauler && y<=yTauler+grandariaTauler);

    }

    // Controla quan es pitja a la pantalla
    // Calcula la casells i si hi ha una peça, la comença a arrossegar

    public void handleInput(int x, int y)
    {
        Point s = casellaSeleccionada(x,y);
        //System.out.println("Casella pitjada: "+s.x+","+s.y);

        fila = s.x;
        columna = s.y;

        if (esCasellaValida(s) && t.casella[fila][columna]%2 == quiJuga)
        {
            xCursor=x;
            yCursor=y;
            Point p = new Point(xTauler + columna * grandariaCasella,
                    yTauler + fila * grandariaCasella);

            despX = x-p.x;
            despY = y-p.y;
            arrossegant = true;
            fitxa = t.casella[fila][columna];
            t.posaPeça(fila, columna, TRANSPARENT);
        }
        else arrossegant = false;
    }

    // Comprova si un moviment es pot fer
    public boolean esMovimentCorrecte(int ox, int oy, int dx, int dy, int c)
    {
        int vo=ox*10+oy;
        int vd=dx*10+dy;

        return trobaMoviment(vo, vd, movimentsPeces[c]);
    }

    // Cerca si hi ha cap moviment d'una casella origen "o" a una destí "d"
    public boolean trobaMoviment(int o, int d, Moviments m) {
        for (int i = 0; i < m.numMoviments; i++) {
            // moviment senzill o captura senzilla
            if (m.moviments[i].origen == o && m.moviments[i].desti == d)
                return true;
        }
        return false;
    }

    // És una dama ? (codi 2 o 3)
    public static boolean dama(int f)
    {
        return f>1;
    }

    // Fa el moviment "m" sobre el tauler
    public void fesMoviment(Moviment m)
    {
        int origenX = m.origen/10;
        int origenY = m.origen%10;
        int destiX  = m.desti/10;
        int destiY  = m.desti%10;
        int color = t.casella[origenX][origenY]%2;
        int fitxa = t.casella[origenX][origenY];

        if (destiX == filaFinal[color] && !dama(fitxa))
            t.posaPeça(destiX, destiY,fitxa+2); // promoció a dama
        else
            t.posaPeça(destiX, destiY,fitxa);

        t.posaPeça(origenX, origenY, BUIDA);
        if (m.salt) {
            // Si és una captura, hem d'eliminar la peça del mig
            t.posaPeça((origenX + destiX) / 2, (origenY + destiY) / 2, BUIDA);
            int i=0;
            while (m.cami[i] != GeneradorDeMoviments.INV) {
                // Si és una captura múltiple, hem de continuar movent
                origenX = destiX;
                origenY = destiY;
                destiX = m.cami[i]/10;
                destiY = m.cami[i]%10;
                t.posaPeça((origenX + destiX) / 2, (origenY + destiY) / 2, BUIDA);
                t.posaPeça(origenX, origenY, BUIDA);
                if (destiX == filaFinal[color] && !dama(fitxa))
                    t.posaPeça(destiX, destiY,fitxa+2); // promoció a dama
                else
                    t.posaPeça(destiX, destiY,fitxa);
                i++;
            }
        }
    }

    // Simula el moviment "m" del tauler t
    // torna el nou tauler modificat
    public static Tauler simulaMoviment(Tauler t, Moviment m)
    {
        Tauler nt = t.duplica();
        int origenX = m.origen/10;
        int origenY = m.origen%10;
        int destiX  = m.desti/10;
        int destiY  = m.desti%10;
        int color = t.casella[origenX][origenY]%2;
        int fitxa = t.casella[origenX][origenY];

        if (destiX == filaFinal[color] && !dama(fitxa))
            nt.posaPeça(destiX, destiY,fitxa+2); // promoció a dama
        else
            nt.posaPeça(destiX, destiY,fitxa);

        nt.posaPeça(origenX, origenY, BUIDA);
        if (m.salt) {
            // Si és una captura, hem d'eliminar la peça del mig
            nt.posaPeça((origenX + destiX) / 2, (origenY + destiY) / 2, BUIDA);
            int i=0;
            while (m.cami[i] != GeneradorDeMoviments.INV) {
                // Si és una captura múltiple, hem de continuar movent
                origenX = destiX;
                origenY = destiY;
                destiX = m.cami[i]/10;
                destiY = m.cami[i]%10;
                nt.posaPeça((origenX + destiX) / 2, (origenY + destiY) / 2, BUIDA);
                nt.posaPeça(origenX, origenY, BUIDA);
                if (destiX == filaFinal[color] && !dama(fitxa))
                    nt.posaPeça(destiX, destiY,fitxa+2); // promoció a dama
                else
                    nt.posaPeça(destiX, destiY,fitxa);
                i++;
            }
        }
        return nt;
    }

    // Controla quan es deixa de pitjar la pantalla
    // si s'estava arrossegant una peça is mou a una casella vàlida,
    // s'ha de fer el moviment i canviar el torn
    public void handleStopInput(int x, int y)
    {
        Point s = casellaSeleccionada(x,y);
        boolean correcte = false;

        if (arrossegant)
        {
            int colorFitxa = fitxa%2;
            if (dinsTauler(x,y) && esCasellaValida(s) && t.casella[s.x][s.y]==BUIDA)  // mou la fitxa de lloc
            {
                if (esMovimentCorrecte(fila, columna, s.x, s.y, colorFitxa)) {
                    //System.out.println("Casilla: " + s.x + "," + s.y + " " + esCasellaValida(s) + "  " + t.casella[s.x][s.y]);

                    if (s.x == filaFinal[colorFitxa] && !dama(fitxa))
                        t.posaPeça(s.x, s.y,fitxa+2); // promoció a dama
                    else
                        t.posaPeça(s.x, s.y,fitxa);

                    t.posaPeça(fila, columna, BUIDA);

                    darrerMoviment.origen = fila*10+columna;
                    darrerMoviment.desti  = s.x*10+s.y;

                    xCursor = x;
                    yCursor = y;
                    // elimina la del mig si és necessari !
                    if (Math.abs(fila-s.x) > 1) {
                        t.posaPeça((fila + s.x) / 2, (columna + s.y) / 2, BUIDA);
                        // si mata, obliga a matar mentre pugui la mateixa fitxa
                        if (s.x != filaFinal[colorFitxa]) {
                            obligaMoviment = true;
                            movimentObligat = s.x * 10 + s.y;
                            //mantenim el torn mentre pugui matar amb la mateixa fitxa

                            movimentsPeces[colorFitxa] = g.generaMovimentsPeces(t, colorFitxa);
                            // deixa només els moviments de captura que pot fer aquesta fitxa
                            int idx = 0;
                            for (int i = 0; i < movimentsPeces[colorFitxa].numMoviments; i++) {
                                if (movimentsPeces[colorFitxa].moviments[i].origen == movimentObligat) {
                                    int o = movimentsPeces[colorFitxa].moviments[i].origen;
                                    int d = movimentsPeces[colorFitxa].moviments[i].desti;
                                    if (Math.abs(d / 10 - o / 10) > 1) // ha de ser una captura (bot)
                                    {
                                        movimentsPeces[colorFitxa].moviments[idx].origen = o;
                                        movimentsPeces[colorFitxa].moviments[idx].desti = d;
                                        idx++;
                                    }
                                }
                            }
                            movimentsPeces[colorFitxa].numMoviments = idx;
                            arrossegant = false;
                            if (idx == 0) // seqüència acabada, pot canviar el torn
                            {
                                //movimentsPeces[fitxa] = g.generaMovimentsPeces(t, fitxa);
                                obligaMoviment = false;
                                canviaTorn();
                            } else return;
                        }
                        else canviaTorn();
                    }
                    else canviaTorn();

                    System.out.println("De "+fila+":"+columna+" a "+s.x+":"+s.y);
                    movimentsPeces[BLANCA] = g.generaMovimentsPeces(t, BLANCA);
                    movimentsPeces[NEGRA]  = g.generaMovimentsPeces(t, NEGRA);
                    correcte = true;
                }
            }

            if (!correcte) t.posaPeça(fila, columna, fitxa);

            arrossegant = false;
        }
    }

    // Pasa el torn al jugador rival
    // i si és el torn de l'ordenador, llavors calcula la jugada
    // i fes el moviment automàtic
    private void canviaTorn()
    {
        quiJuga = (quiJuga == BLANCA)? NEGRA:BLANCA;

        if (quiJuga == ordenador) {
            Moviment m = cerca.avaluaTauler(t, quiJuga);
            if (m != null) {
                System.out.println("Moviment de " + m.origen + " a " + m.desti);
                fesMoviment(m);
                darrerMoviment.origen = m.origen;
                darrerMoviment.desti  = m.desti;
                canviaTorn();
                movimentsPeces[BLANCA] = g.generaMovimentsPeces(t, BLANCA);
                movimentsPeces[NEGRA] = g.generaMovimentsPeces(t, NEGRA);
            }
        }
    }

    // Quin és el jugador contrari ? el del 0 és 1, el del 1 és 0
    private int contrari(int qui)
    {
        return (qui+1)%2;
    }

    // Controla el desplaçament per la pantalla
    // únicament s'ha d'actualitzar la posició del cursor
    public void handleMoveInput(int x, int y)
    {
        xCursor = x;
        yCursor = y;
    }

    // Si em triat una casella, quina ?
    public Point casellaSeleccionada(int x, int y)
    {
        int f=-1;
        int c=-1;

        if (dinsTauler(x,y))
        {
            // està dins el tauler, anam a mirar a quina fila i quina columna
            c = (x-xTauler)/grandariaCasella;
            f = (y-yTauler)/grandariaCasella;
        }

        return new Point(f,c);
    }

    // Dibuixa texte centrat horitzontalment a la pantalla i
    // a la posició "y" relativa al top, al centre o al bottom

    public void drawCenteredTextYTOP(Canvas canvas, String txt, int size, int color, int pos)
    {
        drawCenteredText(canvas, txt, size, color, pos);
    }

    public void drawCenteredTextYCENTER(Canvas canvas, String txt, int size, int color, int pos)
    {
        drawCenteredText(canvas, txt, size, color, pos+amplaPantalla);
    }

    public void drawCenteredTextYBOTTOM(Canvas canvas, String txt, int size, int color, int pos)
    {
        drawCenteredText(canvas, txt, size, color, pos+altPantalla);
    }

    public void drawCenteredText(Canvas canvas, String txt, int size, int color, int pos)
    {
        Paint paintText = new Paint();
        paintText.setTypeface(fontJoc);
        paintText.setTextSize(size);

        // Centrat horitzontal
        paintText.setTextAlign(Paint.Align.CENTER);
        paintText.setColor(color);
        paintText.setAlpha(200);

        int xPos = amplaPantalla / 2;
        //int yPos = (int) (pos + ((paintText.descent() + paintText.ascent()))/4) ;
        int yPos = pos;
        canvas.drawText(txt, xPos, yPos, paintText);
    }

    public void setResume()
    {
    }
    public void saveStatus()
    {
    }
    public void recoverStatus()
    {
    }
}