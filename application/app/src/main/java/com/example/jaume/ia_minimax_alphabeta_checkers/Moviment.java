package com.example.jaume.ia_minimax_alphabeta_checkers;

// Valors que es guarden per descriure un moviment
// Origen indica la fila i columna de la posició inicial:
//        -guarda el valor "fila*10+columna"
// Destí  indica la fila i columna de la posició final:
//        -guarda el valor "fila*10+columna"
// Salt és veritat si es tracta d'una captura
// Camí guarda les diferents caselles d'una captura múltiple
//      les captures múltiples acaben en INV (invàlid)

// si origen i destí són -1, llavors el moviment és invàlid

class Moviment
{
    int origen;
    int desti;
    boolean salt;
    boolean dama;
    int[] cami = new int[10];

    public Moviment()
    {
        this(-1,-1);
    }

    public Moviment(int o, int d)
    {
        salt = false;
        dama = false;
        origen = o;
        desti = d;
        for (int i=0; i<10; i++)
            cami[i]=GeneradorDeMoviments.INV;
    }
};