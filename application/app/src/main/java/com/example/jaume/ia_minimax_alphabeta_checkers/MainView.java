package com.example.jaume.ia_minimax_alphabeta_checkers;
import android.content.Context;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class MainView extends SurfaceView implements Runnable {
    private volatile boolean running;
    private Thread gameThread = null;

    // El nostre joc
    private Game game;

    // Superfície de dibuix
    private SurfaceHolder ourHolder;

    // Frames per segon
    long fps =20;

    MainView(Context context, int screenWidth, int screenHeight) {
        super(context);

        // Crea el joc i el holder de la pantalla de dibuix
        ourHolder = getHolder();
        game = new Game(context, this, screenWidth, screenHeight);
    }

    @Override
    public void run() {

        while (running) {
            long startFrameTime = System.currentTimeMillis();

            game.update(fps);
            game.draw(ourHolder);  // Dibuixa la nostra pantalla

            // Calcula els fps d'aquesta iteració
            long timeThisFrame = System.currentTimeMillis() - startFrameTime;
            if (timeThisFrame >= 1) {
                fps = 1000 / timeThisFrame;
            }
        }
    }

    // Espera que acabi el fil si s'atura el joc
    public void pause() {
        running = false;
        game.saveStatus();
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            // Error
        }
    }

    // Crea un nou fil si es restaura el joc
    public void resume() {
        game.recoverStatus();
        game.setResume();
        gameThread = new Thread(this);
        gameThread.start();
        running = true;
    }

    // Gestió de la interacció amb la pantalla
    public boolean onTouchEvent(MotionEvent motionEvent) {
        handleInput(motionEvent);
        return true;
    }

    public void handleInput(MotionEvent motionEvent) {
        int pointerCount = motionEvent.getPointerCount();

        for (int i = 0; i < pointerCount; i++)
        {
            int x = (int) motionEvent.getX(i);
            int y = (int) motionEvent.getY(i);
            int id = motionEvent.getPointerId(i);
            int action = motionEvent.getActionMasked();

            switch (action)
            {
                case MotionEvent.ACTION_DOWN:
                    game.handleInput(x, y);
                    break;
                case MotionEvent.ACTION_UP:
                    game.handleStopInput(x, y);
                    break;
                case MotionEvent.ACTION_POINTER_DOWN:
                    game.handleInput(x, y);
                    break;
                case MotionEvent.ACTION_POINTER_UP:
                    game.handleStopInput(x, y);
                    break;
                case MotionEvent.ACTION_MOVE:
                    game.handleMoveInput(x, y);
                    break;
            }
        }
    }
}

